package sa.osama_alharbi.test_jasper_report.main;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.*;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrinterName;
import java.util.HashMap;

public class TestJasperReportMain {
    public static void main(String[] args) {
        printAllPrintersName();//هذا الكود يطبع لك كل اسماء الطابعات الل عندك
        fillReport_andPrint("POS-80");
    }
    /*
    --------------------------------------------------------------------------------------------------------------------------------
     */
    private static void printAllPrintersName() {
        PrintService[] pservices = PrintServiceLookup.lookupPrintServices(null, null);
        if (pservices.length > 0) {
            for (PrintService ps : pservices) {
                System.out.println(ps.getName());
            }
        }
    }

    /*
    --------------------------------------------------------------------------------------------------------------------------------
     */
    public static void fillReport_andPrint(String psName){
        PrintService psMain = getPrintServiceByPrinterName(psName);
        if(psMain == null){
            return;
        }
        try {
            JasperDesign jasperDesign = JRXmlLoader.load(TestJasperReportMain.class.getResourceAsStream("Report.jrxml"));

            HashMap<String, Object> parameter = new HashMap<String, Object>();

            parameter.put("var_1","osama");
            parameter.put("var_2","alharbi");


            JRDesignSection jrDesignSection = ((JRDesignSection)jasperDesign.getDetailSection());


            if(psMain != null){

                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter,new JREmptyDataSource());
                Printer_PrintReportToPrinter(jasperPrint,psMain);
            }

        } catch (JRException e) {
            e.printStackTrace();
        }

    }

    private static void Printer_PrintReportToPrinter(JasperPrint jasperPrint,PrintService ps) throws JRException {

        PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();

        PrintRequestAttribute jobPrinArea = new MediaPrintableArea(0, 0, jasperPrint.getPageWidth(), jasperPrint.getPageHeight(), MediaPrintableArea.MM);
//        printRequestAttributeSet.add(MediaSizeName.ISO_A2);
        printRequestAttributeSet.add(jobPrinArea);
        printRequestAttributeSet.add(new Copies(1));
        if (jasperPrint.getOrientationValue() == net.sf.jasperreports.engine.type.OrientationEnum.LANDSCAPE) {
            printRequestAttributeSet.add(OrientationRequested.LANDSCAPE);
        } else {
            printRequestAttributeSet.add(OrientationRequested.PORTRAIT);
        }
        PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
        printServiceAttributeSet.add(new PrinterName(ps.getName(), null));

        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
        configuration.setPrintRequestAttributeSet(printRequestAttributeSet);
        configuration.setPrintServiceAttributeSet(printServiceAttributeSet);
        configuration.setDisplayPageDialog(false);
        configuration.setDisplayPrintDialog(false);

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setConfiguration(configuration);

        if(ps != null){
            try{
                exporter.exportReport();
            }catch(Exception e){
                if(e.getMessage().equals("java.awt.print.PrinterAbortException")){

                }else{
                    e.printStackTrace();
                    System.out.println("JasperReport Error: "+e.getMessage());
                }
            }
        }else{
            System.out.println("JasperReport Error: Printer not found!");
        }
    }

    private static PrintService getPrintServiceByPrinterName(String printerName) {
        PrintService[] pservices = PrintServiceLookup.lookupPrintServices(null, null);
        if (pservices.length > 0) {
            for (PrintService ps : pservices) {
                if(printerName.equals(ps.getName())){
                    return ps;
                }
            }
        }
        return null;
    }
}
